let App = {
    fetch: (url, method = 'GET', options=[])=>{
        fetch()
    },
    registerSW: (file) => {
        console.log(file);
    },
    calc: () =>{

    }
}






function registerSW(file){
    if ("serviceWorker" in navigator){
        navigator.serviceWorker.register(file);
    }else{
        console.log("Whoops! Your browser not supported for ServiceWorker!");
    }
}
function calc(){
    let resultBtn = document.querySelector("#result");
    let clearBtn  = document.querySelector("#clear");
    let resultText= document.getElementsByTagName("input")[0];
    let numbers   = [document.getElementsByTagName("input")[1], document.getElementsByTagName("input")[2]];

    // onChange Event
    // Reset to blank if user input !number
    const inputs = document.getElementsByClassName("number");
    for ( const input of inputs){
        input.addEventListener("input", v => {
            if (!Number.isInteger(parseInt(v.data))){
                input.value = input.value.substring(0, input.value.length-1);
            }
        })
    }


    resultBtn.addEventListener("click", event => {
        if (numbers[0].value !== "" && numbers[1].value !== ""){
            const result  = parseFloat(numbers[0].value) + parseFloat(numbers[1].value);
            return resultText.value = result;
        }
        alert("Can't blank!");

    });

    clearBtn.addEventListener("click", event =>{
        for (const input of inputs){
            input.value = "";
            resultText.value = "";
        }
    })

}

window.addEventListener("load", function () {
   registerSW("sw.js");

   let url = new URL(window.location.href);
   switch (url.pathname){
       case "/calc.html":
           calc();
           break;
       case "/resto.html":
           break;
       default:
           break;
   }
});
