let navBtn   = document.querySelector("button.nav");
let slidebar = document.querySelector(".sidebar");
let slidebar_wrap = document.querySelector(".sidebar-wrapper");
function toggleSideBar(){
    if (slidebar.className.indexOf("open") > -1 && slidebar_wrap.className.indexOf("active") > -1){
        slidebar.classList.remove("open");
        slidebar_wrap.classList.remove("active");
        return;
    }
    slidebar.classList.add("open");
    slidebar_wrap.classList.add("active");
}
navBtn.addEventListener("click",toggleSideBar);
slidebar_wrap.addEventListener("click", toggleSideBar);

let data_source = "http://127.0.0.1:9999/assets/data.json";
let Maps = {
    "data": null,
    "_properties": {
        "token": "pk.eyJ1IjoiZGltYXNyaXNtYWlsIiwiYSI6ImNqbHFnYW5qbjJmcnEzcXM2aDFpejBnaDMifQ._agNfKt3okhEERdoLBm22Q",
        "levelZoom": 17,
        "urlTemplate": "https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}"
    },
    "get": async (id)=>{
        const response = await fetch(data_source);
        const dataJSON = await response.json();
        for (const data of dataJSON.data){
            if (id == data.id){
                Maps.data = data;
            }
        }
    },
    "load": async (id)=>{
        // get data
        await Maps.get(id);
        // use lib
        let coordinates = [Maps.data.latlng[0], Maps.data.latlng[1]];

        // create custom marker
        let markerIcon = L.icon({
            iconUrl: '/assets/img/icon.map.png',
            iconSize: [28, 48]
        });

        let map = L.map('maps').setView(coordinates,Maps._properties.levelZoom);
        L.tileLayer(Maps._properties.urlTemplate,{
            "maxZoom": 20,
            "id":"mapbox.light",
            accessToken: Maps._properties.token
        }).addTo(map);
        new L.marker(coordinates, {icon: markerIcon}).addTo(map);

        // DOM ELEMENT
        let col = document.createElement("div");
        col.classList.add("row");
        col.innerHTML = `
            <div class="col-3">
                <img class="img-responsive" src="${Maps.data.img.thumb}" id="img-thumbnail">
            </div>
            <div class="col-9">
                <h1 class="name-place">${Maps.data.name}</h1>
                <p class="add-place">${Maps.data.address}</p>
                <p class="desc-place">"${Maps.data.description}"</p>
            </div>
        `;
        document.querySelector("#detail").appendChild(col);
        Loader.hide();
    }
};
let Loader = {
    hide: ()=>{
        setTimeout( function () {
            document.querySelector(".loader").classList.remove("loader");
        }, 1000)
    }
};

function domThumbnail(data) {
    let node = document.createElement("div");
    node.classList.add("col-3");
    node.innerHTML = `
        <div class='card'>
            <div class="thumb">
                <img class="img-responsive" srcset="" src="${data.img.thumb}" alt="${data.name}">            
            </div>      
            <div class="card-body">
                <h1 class="title-thumb">${data.name}</h1><span class="rating-wrapper"><i class="material-icons">stars</i> <span class="rating">${data.rating}</span></span>
                <p class="address-thumb">${data.address}</p>
                <a class="btn btn-primary" href="/resto_detail.html?id=${data.id}">Lihat Map</a>
            </div>
        </div>
    `;
    document.querySelector("#thumb").appendChild(node);
}
async function resto() {
    const response = await fetch(data_source);
    const resJSON  = await response.json();
    for (const data of resJSON.data){
        domThumbnail(data);
    }
    Loader.hide();
}
function calc() {
    let form = document.querySelector("#form-calc");
    form.addEventListener("submit", function (e) {
        e.preventDefault();

        const val1 = document.getElementsByName("value1")[0].value;
        const val2 = document.getElementsByName("value2")[0].value;
        const result = parseFloat(val1) + parseFloat(val2);
        document.getElementsByName("result")[0].value = result;


    });
}
let url = new URL(window.location);
switch (url.pathname){
    case "/resto.html":
        resto();
        break;
    case "/resto_detail.html":
        let id = url.searchParams.get("id");
        if (!id){
            window.location.href = "resto.html";
        }
        Maps.load(id);
        break;
    case "/calc.html":
        calc();
        break;
}

// register sw worker

if ("serviceWorker" in navigator){
    navigator.serviceWorker.register("./sw.js");
} else{
    console.log("Whoops! Your browser not supported for ServiceWorker!");
}