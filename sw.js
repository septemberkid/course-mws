const CACHE_VERSION = "dimas-v1";
const CACHE_FILES   = [
    "/index.html",
    "/calc.html",
    "/resto.html",
    "/resto_detail.html",
    "/assets/css/app.v1.min.css",
    "/assets/js/app.v1.js",
    "https://fonts.googleapis.com/icon?family=Material+Icons",
    "https://fonts.gstatic.com/s/materialicons/v41/flUhRq6tzZclQEJ-Vdg-IuiaDsNcIhQ8tQ.woff2",
    "https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmEU9fBBc4AMP6lQ.woff2",
    "https://fonts.gstatic.com/s/roboto/v18/KFOlCnqEu92Fr1MmWUlfBBc4AMP6lQ.woff2",
    "https://fonts.gstatic.com/s/roboto/v18/KFOmCnqEu92Fr1Mu4mxKKTU1Kg.woff2",
    "https://fonts.googleapis.com/css?family=Roboto:400,500,700"
];

self.addEventListener("install", e => {
    console.info("Service worker is installing...");
    e.waitUntil(
        caches.open(CACHE_VERSION).then(cache => {
            return cache.addAll(CACHE_FILES);
        })
    );
});

self.addEventListener("activate", e => {
    console.info("Service worker is activate...");
    caches.keys().then(cache => {
        cache.map(v => {
            if (v !== CACHE_VERSION){
                return  caches.delete(v);
            }
        })
    })
});

self.addEventListener("fetch", e => {
    console.info("Service worker is fetching...");
    e.respondWith(
        caches.open(CACHE_VERSION).then(cache => {
            return cache.match(e.request).then(response => {
                if(response){
                    return response;
                }

                let fetchRequest = e.request.clone();
                return fetch(fetchRequest).then(res =>{
                    if (res.status !== 200 || res.type !== 'basic' || !res){
                        return res;
                    }
                    let responseToCache = res.clone();
                    caches.open(CACHE_VERSION).then(function(cache) {
                        cache.put(e.request, responseToCache);
                    });

                    return res.clone();
                })
            })
        })
    );
});


